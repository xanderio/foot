#pragma once

#include <stdbool.h>
#include <wayland-client.h>

#include "terminal.h"

extern const struct wl_data_device_listener data_device_listener;
extern const struct zwp_primary_selection_device_v1_listener primary_selection_device_listener;

bool selection_enabled(const struct terminal *term);
void selection_start(
    struct terminal *term, int col, int row, enum selection_kind kind);
void selection_update(struct terminal *term, int col, int row);
void selection_finalize(struct terminal *term, uint32_t serial);
void selection_cancel(struct terminal *term);
void selection_extend(struct terminal *term, int col, int row, uint32_t serial);

bool selection_on_row_in_view(const struct terminal *term, int row_no);

void selection_mark_word(struct terminal *term, int col, int row,
                         bool spaces_only, uint32_t serial);
void selection_mark_row(struct terminal *term, int row, uint32_t serial);

void selection_to_clipboard(struct terminal *term, uint32_t serial);
void selection_from_clipboard(struct terminal *term, uint32_t serial);
void selection_to_primary(struct terminal *term, uint32_t serial);
void selection_from_primary(struct terminal *term);

/* Copy text *to* primary/clipboard */
bool text_to_clipboard(struct terminal *term, char *text, uint32_t serial);
bool text_to_primary(struct terminal *term, char *text, uint32_t serial);

/*
 * Copy text *from* primary/clipboard
 *
 * Note that these are asynchronous; they *will* return
 * immediately. The 'cb' callback will be called 0..n times with
 * clipboard data. When done (or on error), the 'done' callback is
 * called.
 *
 * As such, keep this in mind:
 *  - The 'user' context must not be stack allocated
 * - Don't expect clipboard data to have been received when these
 *   functions return (it will *never* have been received at this
 *   point).
 */
void text_from_clipboard(
    struct terminal *term, uint32_t serial,
    void (*cb)(const char *data, size_t size, void *user),
    void (*done)(void *user), void *user);

void text_from_primary(
    struct terminal *term,
    void (*cb)(const char *data, size_t size, void *user),
    void (*dont)(void *user), void *user);
